(*********************************************************************************)
(*                Stog                                                           *)
(*                                                                               *)
(*    Copyright (C) 2012 Maxence Guesdon. All rights reserved.                   *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU General Public License as                    *)
(*    published by the Free Software Foundation, version 3 of the License.       *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the               *)
(*    GNU Library General Public License for more details.                       *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public                  *)
(*    License along with this program; if not, write to the Free Software        *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    As a special exception, you have permission to link this program           *)
(*    with the OCaml compiler and distribute executables, as long as you         *)
(*    follow the requirements of the GNU GPL in regard to all of the             *)
(*    software in the executable aside from the OCaml compiler.                  *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Predefined tags. *)

val site_title : string
val site_desc : string
val site_url : string
val site_email : string
val rss_length : string
val languages : string
val functions : string

val elt : string
val elt_body : string
val elt_date : string
val elt_keywords : string
val elt_hid : string
val elt_intro : string
val elt_src : string
val elt_title : string
val elt_topics : string
val elt_type : string
val elt_url : string

val sep : string

val archive_tree : string
val block : string
val command_line : string
val counter : string
val elements : string
val ext_a : string
val graph : string
val hcode : string
val icode : string
val if_ : string
val ifdef : string
val image : string
val inc : string
val include_ : string
val keyword : string
val langswitch : string
val latex : string
val list : string
val n_columns : string
val next : string
val ocaml : string
val ocaml_eval : string
val page : string
val post : string
val prepare_toc : string
val previous : string
val search_form : string
val section : string
val subsection : string
val toc : string
val topic : string
val two_columns : string


